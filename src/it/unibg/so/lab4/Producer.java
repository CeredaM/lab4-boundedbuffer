package it.unibg.so.lab4;

public class Producer implements Runnable {
	private  Buffer<Integer> buffer;
	   
	public Producer(Buffer<Integer> b) {
		buffer = b;
	}
	
	@Override
	public void run() {
	
		while (true) {
			System.out.println("Producer sleeping.");
			SleepUtilities.sleep();
	
			// produce an item and insert it into the buffer
			Integer i = 0;
			System.out.println("Producer produced " + i);
	
			try {
				buffer.insert(i++);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
