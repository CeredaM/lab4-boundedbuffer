package it.unibg.so.lab4;

public class Driver {
	
	public static void main(String[] args) {
        Buffer<Integer> buffer = new BoundedBuffer<Integer>();
 
        // now create the producer and consumer threads
        Thread producerThread = new Thread(new Producer(buffer));
        Thread consumerThread = new Thread(new Consumer(buffer));
 
        producerThread.start();
        consumerThread.start();
    }
}
