package it.unibg.so.lab4;

public class Consumer implements Runnable {	

	private Buffer<Integer> buffer;
	
	public Consumer(Buffer<Integer> b) {
		buffer = b;
	}

	public void run() {

		while (true) {
			System.out.println("Consumer sleeping.");
			SleepUtilities.sleep();

			// consume an item from the buffer
			System.out.println("Consumer wants to consume.");

			try {
				Integer i = buffer.remove();
				System.out.println("Consumer consumed: " + i);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
