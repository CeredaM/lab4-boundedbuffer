package it.unibg.so.lab4;

public interface Buffer<T>
{
    /**
     * insert an item into the Buffer.
     * Note this may be either a blocking
     * or non-blocking operation.
     */
    public abstract void insert(T item) throws InterruptedException;
 
    /**
     * remove an item from the Buffer.
     * Note this may be either a blocking
     * or non-blocking operation.
     */
    public abstract T remove() throws InterruptedException;
}
