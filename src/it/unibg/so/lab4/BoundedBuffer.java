package it.unibg.so.lab4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BoundedBuffer<T> implements Buffer<T> {

	private static final int BUFFER_SIZE = 5;

	private int count; // number of items in the buffer
	private int in; // points to the next free position in the buffer
	private int out; // points to the next full position in the buffer
	private List<T> buffer;

	// Condition variables
	final Lock lock = new ReentrantLock();
	final Condition notFull = lock.newCondition();
	final Condition notEmpty = lock.newCondition();

	public BoundedBuffer() {
		// buffer is initially empty
		count = 0;
		in = 0;
		out = 0;
		buffer = new ArrayList<T>(BUFFER_SIZE);
	}

	@Override
	public void insert(T item) throws InterruptedException {
		lock.lock();
		System.out.println("[Producer] Lock acquired.");
		try {
			while (count == BUFFER_SIZE)
				notFull.await();
			// add an item to the buffer
			count++;
			buffer.add(in, item);
			in = (in + 1) % BUFFER_SIZE;
			if (count == BUFFER_SIZE)
				System.out.println("[Producer] producing " + item + " Buffer FULL");
			else
				System.out.println("[Producer] producing " + item + " Buffer Size = " + count);

			notEmpty.signal();
		} finally {
			System.out.println("[Producer] releasing the Lock.");
			lock.unlock();
		}

	}

	@Override
	public T remove() throws InterruptedException {
		lock.lock();
		System.out.println("[Consumer] Lock acquired.");
		try {
			while (count == 0)
				notEmpty.await();

			count--;
			T item = buffer.get(out);
			out = (out + 1) % BUFFER_SIZE;

			if (count == 0)
				System.out.println("[Consumer] consuming " + item + " Buffer EMPTY");
			else
				System.out.println("[Consumer] consuming " + item + " Buffer Size = " + count);

			notFull.signal();
			return item;
		} finally {
			System.out.println("[Consumer] releasing the Lock.");
			lock.unlock();
		}
	}

}
